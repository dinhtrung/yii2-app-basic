<?php
/**
Front end:
	/user/registration/register Displays registration form
	/user/registration/resend Displays resend form
	/user/registration/confirm Confirms a user (requires id and token query params)
	/user/security/login Displays login form
	/user/recovery/request Displays recovery request form
	/user/recovery/reset Displays password reset form (requires id and token query params)
Backend:
	/user/security/logout Logs the user out (available only via POST method)
	/user/settings/profile Displays profile settings form
	/user/settings/account Displays account settings form (email, username, password)
	/user/settings/networks Displays social network accounts settings page
	/user/profile/show Displays user's profile (requires id query param)
With Privileges:
	/user/admin/index Displays user management interface

 */
if (Yii::$app->user->isGuest)
	return [
		['label' => Yii::t('app', 'Login'), 'url'=>['/user/security/login']],
		['label' => Yii::t('app', 'Sign Up'), 'url'=>['/user/registration/register']],
		['label' => Yii::t('app', 'Forgot Password'), 'url'=>['/user/registration/resend']],

	];
else
	return [
		['label' => Yii::t('app', 'User Management'), 'url'=>['/user/admin/index'], 'visible' => \Yii::$app->user->can('/user/admin/index')],
		['label' => Yii::t('app', 'Logout'), 'url'=>['/user/security/logout']],
		['label' => Yii::t('app', 'Profile'), 'url'=>['/user/settings/profile']],
		['label' => Yii::t('app', 'Account'), 'url'=>['/user/settings/account']],
		['label' => Yii::t('app', 'Networks'), 'url'=>['/user/settings/networks']],

	];