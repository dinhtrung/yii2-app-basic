<?php

namespace app\components;
use yii\web\User as BaseUser;
use Yii;

class User extends BaseUser
{
	/**
	 * Override `can()` method to support recursive checking based on route.
	 * @see yii\web\User
	 * @param string $permissionName
	 * 		The permissionName should be the route to check against. For example, if check against '/module1/submodule2/controller/action', we will check also '/module1/submodule2/controller/*' , '/module1/submodule2/*' , '/module1/*' and '/*'.
	 * @param array $params
	 * @param string $allowCaching
	 * @return boolean $access
	 * 		true if user can access the menu.
	 */
	public function can($permissionName, $params = [], $allowCaching = true)
	{
		Yii::trace("Checking permission of $permissionName", "can");
		$access = false;
		if ($access = parent::can($permissionName, $params, $allowCaching))
			return $access;
		else {
			do {
				Yii::trace("Checking extra permission of $permissionName", "can");
				$permissionName = substr($permissionName, 0, strrpos($permissionName, '/'));
			} while (!($access = parent::can($permissionName . '/*', $params, $allowCaching)) && ($permissionName));
			return $access;
		}
	}
}
